
package com.example.consumingwebservice.clientModel;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.ws.client.core.support.WebServiceGatewaySupport;
import org.springframework.ws.soap.client.core.SoapActionCallback;

import com.example.consumingwebservice.wsdl.GetMedicationRequest;
import com.example.consumingwebservice.wsdl.GetMedicationResponse;


public class MedicationClient extends WebServiceGatewaySupport {

    private static final Logger log = LoggerFactory.getLogger(MedicationClient.class);

    public GetMedicationResponse getMedication(String idPatient) {

        GetMedicationRequest request = new GetMedicationRequest();
        request.setPatientId(idPatient);

        log.info("Requesting location for " + idPatient);

        GetMedicationResponse response = (GetMedicationResponse) getWebServiceTemplate()
                .marshalSendAndReceive("http://localhost:8092/ws/countries", request,
                        new SoapActionCallback(
                                "http://spring.io/guides/gs-producing-web-service/GetMedicationRequest"));

        return response;
    }

}
