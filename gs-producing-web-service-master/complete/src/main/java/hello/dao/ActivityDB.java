package hello.dao;

import hello.model.Activity;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

public class ActivityDB {

    Statement statement = null;
    Connection connection = null;
    ResultSet resultSet = null;
    PreparedStatement findStatement = null;


    private static final Logger LOGGER = Logger.getLogger(ActivityDB.class.getName());
    private static final String DRIVER = "com.mysql.jdbc.Driver";
    private static final String DBURL = "jdbc:mysql://localhost:3306/test";
    private static final String USER = "root";


    public List<Activity> printAllActivitiesForPatientId(int idPatient )  {

        String  query = "SELECT * FROM activity where patientId = ?";
        List<Activity> activityList = new ArrayList();

        ResultSet rs = null;

        try {

            Class.forName(DRIVER);
            Connection connection = DriverManager.getConnection(DBURL, USER, "");

            findStatement = connection.prepareStatement(query);
            findStatement.setInt(1, idPatient);
            resultSet = findStatement.executeQuery();

            while (resultSet.next()) {

                Activity currentActivity = new Activity();
                int id = resultSet.getInt("id");
                String start_time = resultSet.getString("start_time");
                String end_time = resultSet.getString("end_time");
                String label= resultSet.getString("label");


                currentActivity.setId(resultSet.getInt("id"));
                currentActivity.setStart_time(resultSet.getString("start_time"));
                currentActivity.setEnd_time(resultSet.getString("end_time"));
                currentActivity.setLabel(resultSet.getString("label"));
                currentActivity.setObservations(resultSet.getString("observations"));
                currentActivity.setPatientId(resultSet.getInt("patientId"));


                // print the results
                   LOGGER.info( id+ " "+ start_time+" "+ end_time+" "+ label+" ");
                activityList.add(currentActivity);
            }

        } catch (SQLException | ClassNotFoundException e) {
            e.getCause();
        }
        return activityList;
    }


}
