package hello.model;

public class Medication {

    private int medicationId;
    private String name;
    private String sideEffects;
    private Integer dosage;
    private int patientId;
    private int prescriptionId;
    private Integer intokeInterval;
    private Integer periodTreatment;
    private String tokenPill;

    public String getMedicationId() {
        return String.valueOf(medicationId);
    }

    public void setMedicationId(int medicationId) {
        this.medicationId = medicationId;
    }

    public String getPrescriptionId() {
        return String.valueOf(prescriptionId);
    }

    public void setPrescriptionId(int prescriptionId) {
        this.prescriptionId = prescriptionId;
    }

    public String getIntokeInterval() {
        return String.valueOf(intokeInterval);
    }

    public void setIntokeInterval(Integer intokeInterval) {
        this.intokeInterval = intokeInterval;
    }

    public String getPeriodTreatment() {
        return String.valueOf(periodTreatment);
    }

    public void setPeriodTreatment(Integer periodTreatment) {
        this.periodTreatment = periodTreatment;
    }

    public String getTokenPill() {
        return tokenPill;
    }

    public void setTokenPill(String tokenPill) {
        this.tokenPill = tokenPill;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSideEffects() {
        return sideEffects;
    }

    public void setSideEffects(String sideEffects) {
        this.sideEffects = sideEffects;
    }

    public String getDosage() {
        return String.valueOf(dosage);
    }

    public void setDosage(int dosage) {
        this.dosage = dosage;
    }

    public void setDosage(Integer dosage) {
        this.dosage = dosage;
    }

    public String getPatientId() {
        return String.valueOf(patientId);
    }

    public void setPatientId(int patientId) {
        this.patientId = patientId;
    }


}
