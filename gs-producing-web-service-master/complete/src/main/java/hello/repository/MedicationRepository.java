package hello.repository;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

import hello.dao.MedicationDB;
import io.spring.guides.gs_producing_web_service.Medication;
import io.spring.guides.gs_producing_web_service.MedicationList;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class MedicationRepository {
    private static final List<Medication> medications = new ArrayList<Medication>();

    @PostConstruct

    public void initData() {

        MedicationDB medBD= new MedicationDB();
        List<hello.model.Medication> medicationList = medBD.printAllMedicationsForPatientId(1);

        for(hello.model.Medication it : medicationList) {
            Medication medication = new Medication();
            medication.setPatientId(it.getPatientId());
            medication.setName(it.getName());
            medication.setDosage(it.getDosage());
            medication.setSideEffects(it.getSideEffects());
            medication.setMedicationId(it.getMedicationId());
            medication.setIntokeInterval(it.getIntokeInterval());
            medication.setPeriodTreatment(it.getPeriodTreatment());
            medication.setTokenPill(it.getTokenPill());
            medication.setIdPrescription(it.getPrescriptionId());


//            PrescriptionList pc= new PrescriptionList();
//            pc.setPrescription(it.getPrescription());
//
//            medication.setPrescriptionList(pc);
            medications.add(medication);
        }

    }

    public MedicationList findMedication(String patientId) {
        MedicationList al= new MedicationList(medications);
        return al;
    }
}
